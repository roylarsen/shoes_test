#!/usr/bin/python2.7

import linecache, argparse, sys, os, re, datetime

def get_time():
  '''
  utility function to get the current time, then time for 10 minutes ago 
  and an hour ago for formatted time and epoch time
  '''
  time = datetime.datetime.now()
  time_10 = (time - datetime.timedelta(minutes=10)).strftime('%b %d %H:%M:%S')
  time_hr = (time - datetime.timedelta(hours=1)).strftime('%b %d %H:%M:%S')
  epoch_10 = (time - datetime.timedelta(minutes=10)).strftime('%s')
  epoch_hr = (time - datetime.timedelta(hours=1)).strftime('%s')

  return {"time_10":time_10, "time_hr":time_hr, "epoch_10":epoch_10, "epoch_hr":epoch_hr}

def process_log(logfile):
  times = get_time()
  if os.path.isfile(logfile):
    # gets file length to abuse linecache.getline() in a reverse for loop
    length = sum(1 for line in open(logfile))
    for i in range(length, 0, -1):
      line = linecache.getline(logfile, i)
      # this section looks for non-epoch timestamp
      if re.match(r'([A-Z]{1}[a-z]{2})', line):
        ''' 
        these lines take non-epoch lines splits them them to date/time elements and message
        then joins the date/time elements to have a list of date/time and message
        '''
        parsed = line.split(' ', 3)
        parsed[0:3] = [' '.join(parsed[0:3])]
        if parsed[0] > times['time_10']:
          if 'WARNING' in parsed[1]:
            print "WARNING - %s" % line.strip()
            sys.exit(1)
          elif 'ERROR' in parsed[1]:
            print "ERROR - %s" % line.strip()
            sys.exit(2)
        elif parsed[0] <= times['time_10'] and parsed[0] >= times['time_hr']:
          # If there's logs that are fine, we should just continue as normal
          pass
        elif parsed[0] < times['time_hr']:
          print "WARNING - No Logs in last hour"
          sys.exit(1)
      # this will handle epoch time
      elif re.match(r'([0-9]+\s{1})', line):
        # split each line to timestamp and message
        parsed = line.split(' ', 1)
        if parsed[0] > times['epoch_10']:
          if 'WARNING' in parsed[1]:
            print "WARNING - %s" % line.strip()
            sys.exit(1)
          elif 'ERROR' in parsed[1]:
            print "ERROR - %s" % line.strip()
            sys.exit(2)
        elif parsed[0] <= times['epoch_10'] and parsed[0] >= times['epoch_hr']:
          # logs are present within the last hour
          pass
        elif parsed[0] < times['epoch_hr']:
          print "WARNING - No logs in last hour"
          sys.exit(1)
  else:
    print "ERROR - No such file"
    sys.exit(2)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-f', help='path to logfile for parsing')
  args = parser.parse_args()
 
  if args.f is None:
    parser.print_help()
    sys.exit(1)
  
  process_log(args.f)
