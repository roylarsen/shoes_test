# Log Check

Python check for Sensu/Nagios for monitoring log files.

Specs:
1. Produce a warning if there are no entries in the last hour
2. Produce a warning if there is a log entry with the word WARNING in it in the last 10 minutes
3. Produce an error if the log doesn't exist
4. Produce an error if there is a log entry with the word ERROR in it in the last 10 minutes

Also must handle time in the following formats:

* Feb 15 11:40:15
* Epoch notation

## Usage
```
 $ python log_check.py -h
   usage: log_check.py [-h] [-f F]
 
   optional arguments:
     -h, --help  show this help message and exit
     -f F        path to logfile for parsing
```
